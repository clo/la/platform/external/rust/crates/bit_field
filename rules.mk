# This file is generated by cargo_embargo.
# Do not modify this file after the LOCAL_DIR line
# because the changes will be overridden on upgrade.
# Content before the first line starting with LOCAL_DIR is preserved.

LOCAL_DIR := $(GET_LOCAL_DIR)
MODULE := $(LOCAL_DIR)
MODULE_CRATE_NAME := bit_field
MODULE_RUST_CRATE_TYPES := rlib
MODULE_SRCS := $(LOCAL_DIR)/src/lib.rs
MODULE_RUST_EDITION := 2015
MODULE_LIBRARY_DEPS := \
	

include make/library.mk
